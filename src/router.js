import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Cart from './views/Cart.vue'
import Checkout from './views/Checkout.vue'
import DetailProduct from './views/DetailProduct.vue'
import Product from './views/Product.vue'
import OrderList from './views/OrderList.vue'
import Invoice from './views/Invoice.vue'
import store from './store'

Vue.use(Router)

//export default new Router({
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: Checkout
    },
    {
      path: '/detailproduct',
      name: 'detail.product',
      component: DetailProduct
    },
    {
      path: '/product',
      name: 'product',
      component: Product
    },
    {
      path: '/orderlist',
      name: 'orderlist',
      component: OrderList
    },
    {
      path: '/invoice',
      name: 'invoice',
      component: Invoice
    },
  ]
})

router.beforeEach((to, from, next) => {
  // jika routing ada meta auth-nya maka
  if (to.matched.some(record => record.meta.auth)) {
    // jika user adalah guest
    if(store.getters['auth/guest']){
      // tampilkan pesan bahwa harus login dulu
      store.dispatch('alert/set', {
        status : true,
        text  : 'Login first',
        color  : 'error',
      })
      store.dispatch('setPrevUrl', to.path) 
      // tampilkan form login
      store.dispatch('dialog/setComponent', 'login')
    }
    else{
      next()
    } 
  }
  else{
      next()
  }
})

export default router;